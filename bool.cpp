#include "bool.h"

using namespace sed::wrap;

Bool::Bool(bool value) :
    _value(new bool(value))
{ }

Bool::Bool(const Bool *boolObject) {
    if ( boolObject == nullptr ){
        _value = new bool(0);
    } else {
        _value = new bool(boolObject->getValue());
    }
}

Bool::Bool(const Bool &cpy) :
    _value(new bool(cpy.getValue()))
{ }

Bool& Bool::operator =(const Bool &ma){
    _value = new bool(ma.getValue());
    return *this;
}

Bool::~Bool(){
    delete _value;
    _value = nullptr;
}

Bool Bool::parseBool(const std::string &obj){
    if ( obj == "1" || obj == "true" || obj == "TRUE" ){
        return wrap::Bool(true);
    } else if ( obj == "0" || obj == "false" || obj == "FALSE" ){
        return wrap::Bool(false);
    } else {
        return wrap::Bool(false);
    }
}

std::string Bool::boolToString(){
    if ( *_value == true ){
        return std::string(std::to_string(1));
    } else {
        return std::string(std::to_string(0));
    }
}

std::string Bool::boolToString(const bool value){
    if ( value == true ){
        return std::string(std::to_string(1));
    } else {
        return std::string(std::to_string(0));
    }
}

void Bool::deleteWrapper(){
    delete _value;
    _value = nullptr;
}

bool Bool::getValue() const {
    return *(_value);
}
