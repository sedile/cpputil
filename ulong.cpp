#include <stdexcept>

#include "ulong.h"

using namespace sed::wrap;

ULong::ULong(unsigned long long value) :
    _value(new unsigned long long(value))
{ }

ULong::ULong(const ULong *ulongObject) {
    if ( ulongObject == nullptr ){
        _value = new unsigned long long(0);
    } else {
        _value = new unsigned long long(ulongObject->getValue());
    }
}

ULong::ULong(const ULong &cpy) :
    _value(new unsigned long long(cpy.getValue()))
{ }

ULong& ULong::operator =(const ULong &ma){
    _value = new unsigned long long(ma.getValue());
    return *this;
}

ULong::~ULong(){
    delete _value;
    _value = nullptr;
}

std::string ULong::ulongToString(){
    return std::string(std::to_string(*_value));
}

std::string ULong::ulongToString(const unsigned long long value){
    return std::string(std::to_string(value));
}

ULong ULong::parseULong(const UShort &obj){
    return wrap::ULong(static_cast<unsigned long long>(obj.getValue()));
}

ULong ULong::parseULong(const UInteger &obj){
    return wrap::ULong(static_cast<unsigned long long>(obj.getValue()));
}

ULong ULong::parseULong(const std::string &obj){
    unsigned long long x = 0;
    try {
        x = std::stoll(obj);
        return wrap::ULong(x);
    } catch (const std::invalid_argument &){
        return wrap::ULong(x);
    } catch (const std::out_of_range &){
        return wrap::ULong(x);
    }
}

void ULong::deleteWrapper(){
    delete _value;
    _value = nullptr;
}

unsigned long long ULong::getValue() const {
    return *(_value);
}
