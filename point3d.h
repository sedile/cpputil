#ifndef POINT3D_H
#define POINT3D_H

#include "point2d.h"

namespace sed {

    namespace geometry {

        template<typename T = int>
        class Point3D {
        private:
            T _x;
            T _y;
            T _z;
        public:
            explicit Point3D();
            explicit Point3D(T x, T y);
            explicit Point3D(T x, T y, T z);
            Point3D(const Point3D<T> &cpy);
            Point3D<T>& operator =(const Point3D<T> &mo) = delete;

            virtual ~Point3D();

            void setX(T x);
            void setY(T y);
            void setZ(T z);

            Point2D<T> transformto2D();

            T getX() const;
            T getY() const;
            T getZ() const;

        };

        template<typename T>
        Point3D<T>::Point3D() :
            _x(0), _y(0), _z(0)
        { }

        template<typename T>
        Point3D<T>::Point3D(T x, T y) :
            _x(x), _y(y), _z(0)
        { }

        template<typename T>
        Point3D<T>::Point3D(T x, T y, T z) :
            _x(x), _y(y), _z(z)
        { }

        template<typename T>
        Point3D<T>::Point3D(const Point3D<T> &cpy) :
            _x(cpy.getX()), _y(cpy.getY())
        { }

        template<typename T>
        Point3D<T>::~Point3D() { }

        template<typename T>
        Point2D<T> Point3D<T>::transformto2D(){
            return Point2D<T>(_x,_y);
        }

        template<typename T>
        void Point3D<T>::setX(const T x){
            _x = x;
        }

        template<typename T>
        void Point3D<T>::setY(const T y){
            _y = y;
        }

        template<typename T>
        void Point3D<T>::setZ(const T z){
            _z = z;
        }

        template<typename T>
        T Point3D<T>::getX() const {
            return _x;
        }

        template<typename T>
        T Point3D<T>::getY() const {
            return _y;
        }

        template<typename T>
        T Point3D<T>::getZ() const {
            return _z;
        }
    }
}

#endif // POINT3D_H
