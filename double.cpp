#include <stdexcept>

#include "double.h"

using namespace sed::wrap;

Double::Double(double value) :
    _value(new double(value))
{ }

Double::Double(const Double *doubleObject) {
    if ( doubleObject == nullptr ){
        _value = new double(0.0);
    } else {
        _value = new double(doubleObject->getValue());
    }
}

Double::Double(const Double &cpy) :
    _value(new double(cpy.getValue()))
{ }

Double& Double::operator =(const Double &ma){
    _value = new double(ma.getValue());
    return *this;
}

Double::~Double(){
    delete _value;
    _value = nullptr;
}

std::string Double::doubleToString(){
    return std::string(std::to_string(*_value));
}

std::string Double::doubleToString(const double value){
    return std::string(std::to_string(value));
}

Double Double::parseDouble(const Float &obj){
    return wrap::Double(static_cast<double>(obj.getValue()));
}

Double Double::parseDouble(const std::string &obj){
    double x = 0.0;
    try {
        x = std::stod(obj);
        return wrap::Double(x);
    } catch (const std::invalid_argument &){
        return wrap::Double(x);
    } catch (const std::out_of_range &){
        return wrap::Double(x);
    }
}

void Double::deleteWrapper(){
    delete _value;
    _value = nullptr;
}

double Double::getValue() const {
    return *(_value);
}
