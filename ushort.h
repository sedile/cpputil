#ifndef USHORT_H
#define USHORT_H

#include <string>

namespace sed {

    namespace wrap {

    class UShort {
    private:
        unsigned short *_value;
    public:
        static constexpr const unsigned short MIN_VALUE = 0;
        static constexpr const unsigned short MAX_VALUE = 65535;

        explicit UShort(unsigned short value);
        UShort(const UShort *ushortObject);
        UShort(const UShort &cpy);
        UShort& operator=(const UShort &ca);
        virtual ~UShort();

        std::string ushortToString();

        static std::string ushortToString(const unsigned short value);
        static UShort parseUShort(const std::string &obj);

        void deleteWrapper();
        unsigned short getValue() const;
    };

    }
}

#endif // USHORT_H
