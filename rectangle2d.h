#ifndef RECTANGLE2D_H
#define RECTANGLE2D_H

#include "point2d.h"

namespace sed {

    namespace geometry {

        template<typename T = int>
        class Rectangle2D {
        private:
            Point2D<T> *_topLeft;
            Point2D<T> *_topRight;
            Point2D<T> *_bottomLeft;
            Point2D<T> *_bottomRight;
        public:
            explicit Rectangle2D();
            explicit Rectangle2D(Point2D<T> &topLeft, Point2D<T> &topRight, Point2D<T> &bottomLeft, Point2D<T> &bottomRight);
            explicit Rectangle2D(const Rectangle2D<T> &cpy);
            Rectangle2D<T>& operator=(const Rectangle2D<T> &mo) = delete;

            virtual ~Rectangle2D();

            Point2D<T>* getTopLeftPoint() const;
            Point2D<T>* getTopRightPoint() const;
            Point2D<T>* getBottomLeftPoint() const;
            Point2D<T>* getBottumRightPoint() const;
        };

        template<typename T>
        Rectangle2D<T>::Rectangle2D()
            : _topLeft(new Point2D<T>), _topRight(new Point2D<T>),
              _bottomLeft(new Point2D<T>), _bottomRight(new Point2D<T>)
        { }

        template<typename T>
        Rectangle2D<T>::Rectangle2D(Point2D<T> &topLeft, Point2D<T> &topRight, Point2D<T> &bottomLeft, Point2D<T> &bottomRight)
            : _topLeft(new Point2D<T>(topLeft)), _topRight(new Point2D<T>(topRight)),
              _bottomLeft(new Point2D<T>(bottomLeft)), _bottomRight(new Point2D<T>(bottomRight))
        { }

        template<typename T>
        Rectangle2D<T>::Rectangle2D(const Rectangle2D<T> &cpy)
            : _topLeft(new Point2D<T>(*cpy.getTopLeftPoint())),
              _topRight(new Point2D<T>(*cpy.getTopRightPoint())),
              _bottomLeft(new Point2D<T>(*cpy.getBottomLeftPoint())),
              _bottomRight(new Point2D<T>(*cpy.getBottumRightPoint()))
        { }

        template<typename T>
        Rectangle2D<T>::~Rectangle2D(){
            delete _topLeft;
            delete _topRight;
            delete _bottomLeft;
            delete _bottomRight;
            _topLeft = nullptr;
            _topRight = nullptr;
            _bottomLeft = nullptr;
            _bottomRight = nullptr;
        }

        template<typename T>
        Point2D<T>* Rectangle2D<T>::getTopLeftPoint() const {
            return _topLeft;
        }

        template<typename T>
        Point2D<T>* Rectangle2D<T>::getTopRightPoint() const {
            return _topRight;
        }

        template<typename T>
        Point2D<T>* Rectangle2D<T>::getBottomLeftPoint() const {
            return _bottomLeft;
        }

        template<typename T>
        Point2D<T>* Rectangle2D<T>::getBottumRightPoint() const {
            return _bottomRight;
        }

    }

}

#endif // RECTANGLE2D_H
