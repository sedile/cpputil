#ifndef UINTEGER_H
#define UINTEGER_H

#include "ushort.h"

namespace sed {

    namespace wrap {

    class UInteger {
    private:
        unsigned int *_value;
    public:
        static constexpr const unsigned int MIN_VALUE = 0;
        static constexpr const unsigned int MAX_VALUE = 4294967295;

        explicit UInteger(unsigned int value);
        UInteger(const UInteger *uintegerObject);
        UInteger(const UInteger &cpy);
        UInteger& operator=(const UInteger &ca);
        virtual ~UInteger();

        std::string uintToString();

        static std::string uintToString(const unsigned int value);
        static UInteger parseUInteger(const UShort &obj);
        static UInteger parseUInteger(const std::string &obj);

        void deleteWrapper();
        unsigned int getValue() const;
    };

    }
}

#endif // UINTEGER_H
