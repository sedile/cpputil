#include <stdexcept>

#include "float.h"

using namespace sed::wrap;

Float::Float(float value) :
    _value(new float(value))
{ }

Float::Float(const Float *floatObject) {
    if ( floatObject == nullptr ){
        _value = new float(0.0);
    } else {
        _value = new float(floatObject->getValue());
    }
}

Float::Float(const Float &cpy) :
    _value(new float(cpy.getValue()))
{ }

Float& Float::operator =(const Float &ma){
    _value = new float(ma.getValue());
    return *this;
}

Float::~Float(){
    delete _value;
    _value = nullptr;
}

std::string Float::floatToString(){
    return std::string(std::to_string(*_value));
}

std::string Float::floatToString(const float value){
    return std::string(std::to_string(value));
}

Float Float::parseFloat(const std::string &obj){
    float x = 0.0f;
    try {
        x = std::stof(obj);
        return wrap::Float(x);
    } catch (const std::invalid_argument &){
        return wrap::Float(x);
    } catch (const std::out_of_range &){
        return wrap::Float(x);
    }
}

void Float::deleteWrapper(){
    delete _value;
    _value = nullptr;
}

float Float::getValue() const {
    return *(_value);
}
