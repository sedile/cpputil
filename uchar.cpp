#include "uchar.h"

using namespace sed::wrap;

UChar::UChar(unsigned char value) :
    _value(new unsigned char(value))
{ }

UChar::UChar(const UChar *ucharObject) {
    if ( ucharObject == nullptr ){
        _value = new unsigned char(' ');
    } else {
        _value = new unsigned char(ucharObject->getValue());
    }
}

UChar::UChar(const UChar &cpy) :
    _value(new unsigned char(cpy.getValue()))
{ }

UChar& UChar::operator =(const UChar &ma){
    _value = new unsigned char(ma.getValue());
    return *this;
}

UChar::~UChar(){
    delete _value;
    _value = nullptr;
}

std::string UChar::ucharToString(){
    return std::string(1, *_value);
}

std::string UChar::ucharToString(const unsigned char value){
    return std::string(1, value);
}

void UChar::deleteWrapper(){
    delete _value;
    _value = nullptr;
}

unsigned char UChar::getValue() const {
    return *(_value);
}
