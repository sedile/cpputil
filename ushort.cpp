#include <stdexcept>

#include "ushort.h"

using namespace sed::wrap;

UShort::UShort(unsigned short value) :
    _value(new unsigned short(value))
{ }

UShort::UShort(const UShort *ushortObject) {
    if ( ushortObject == nullptr ){
        _value = new unsigned short(0);
    } else {
        _value = new unsigned short(ushortObject->getValue());
    }
}

UShort::UShort(const UShort &cpy) :
    _value(new unsigned short(cpy.getValue()))
{ }

UShort& UShort::operator =(const UShort &ma){
    _value = new unsigned short(ma.getValue());
    return *this;
}

UShort::~UShort(){
    delete _value;
    _value = nullptr;
}

std::string UShort::ushortToString(){
    return std::string(std::to_string(*_value));
}

std::string UShort::ushortToString(const unsigned short value){
    return std::string(std::to_string(value));
}

UShort UShort::parseUShort(const std::string &obj){
    unsigned short x = 0;
    try {
        x = static_cast<unsigned int>(std::stoull(obj));
        return wrap::UShort(x);
    } catch (const std::invalid_argument &){
        return wrap::UShort(x);
    } catch (const std::out_of_range &){
        return wrap::UShort(x);
    }
}

void UShort::deleteWrapper(){
    delete _value;
    _value = nullptr;
}

unsigned short UShort::getValue() const {
    return *(_value);
}
