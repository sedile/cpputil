#include "char.h"

using namespace sed::wrap;

Char::Char(char value) :
    _value(new char(value))
{ }

Char::Char(const Char *charObject) {
    if ( charObject == nullptr ){
        _value = new char(' ');
    } else {
        _value = new char(charObject->getValue());
    }
}

Char::Char(const Char &cpy) :
    _value(new char(cpy.getValue()))
{ }

Char& Char::operator =(const Char &ma){
    _value = new char(ma.getValue());
    return *this;
}

Char::~Char(){
    delete _value;
    _value = nullptr;
}

std::string Char::charToString(){
    return std::string(1, *_value);
}

std::string Char::charToString(const char value){
    return std::string(1, value);
}

void Char::deleteWrapper(){
    delete _value;
    _value = nullptr;
}

char Char::getValue() const {
    return *(_value);
}
