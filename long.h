#ifndef LONG_H
#define LONG_H

#include "short.h"
#include "ushort.h"
#include "integer.h"
#include "uinteger.h"

namespace sed {

    namespace wrap {

    class Long {
    private:
        long long *_value;
    public:
        static constexpr const long long MIN_VALUE = -9223372036854775808uLL;
        static constexpr const long long MAX_VALUE = 9223372036854775807LL;

        explicit Long(long long value);
        Long(const Long *longObject);
        Long(const Long &cpy);
        Long& operator=(const Long &ca);
        virtual ~Long();

        std::string longToString();

        static std::string longToString(const long value);
        static Long parseLong(const Short &obj);
        static Long parseLong(const UShort &obj);
        static Long parseLong(const Integer &obj);
        static Long parseLong(const UInteger &obj);
        static Long parseLong(const std::string &obj);

        void deleteWrapper();
        long long getValue() const;
    };

    }
}

#endif // LONG_H
