#ifndef CHAR_H
#define CHAR_H

#include "bool.h"

namespace sed {

    namespace wrap {

    class Char {
    private:
        const char *_value;
    public:
        explicit Char(char value);
        Char(const Char *boolObject);
        Char(const Char &cpy);
        Char& operator=(const Char &ca);
        virtual ~Char();

        std::string charToString();

        static std::string charToString(const char value);

        void deleteWrapper();
        char getValue() const;
    };

    }

}

#endif // CHAR_H
