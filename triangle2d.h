#ifndef TRIANGLE2D_H
#define TRIANGLE2D_H

#include <vector>
#include "line2d.h"

namespace sed {

    namespace geometry {

        template<typename T = int>
        class Triangle2D {
        private:
            Point2D<T> *_top;
            Point2D<T> *_bottomLeft;
            Point2D<T> *_bottomRight;
        public:
            explicit Triangle2D();
            explicit Triangle2D(Point2D<T> &top, Point2D<T> &bottomLeft, Point2D<T> &bottomRight);
            explicit Triangle2D(const Triangle2D<T> &cpy);
            Triangle2D<T>& operator=(const Triangle2D<T> &mo) = delete;

            virtual ~Triangle2D();

            Point2D<T>* getTopPoint() const;
            Point2D<T>* getBottomLeftPoint() const;
            Point2D<T>* getBottumRightPoint() const;
        };

        template<typename T>
        Triangle2D<T>::Triangle2D()
            : _top(new Point2D<T>), _bottomLeft(new Point2D<T>), _bottomRight(new Point2D<T>)
        { }

        template<typename T>
        Triangle2D<T>::Triangle2D(Point2D<T> &top, Point2D<T> &bottomLeft, Point2D<T> &bottomRight)
            : _top(new Point2D<T>(top)), _bottomLeft(new Point2D<T>(bottomLeft)), _bottomRight(new Point2D<T>(bottomRight))
        { }

        template<typename T>
        Triangle2D<T>::Triangle2D(const Triangle2D<T> &cpy)
            : _top(new Point2D<T>(*cpy.getTopPoint())),
              _bottomLeft(new Point2D<T>(*cpy.getBottomLeftPoint())),
              _bottomRight(new Point2D<T>(*cpy.getBottumRightPoint()))
        { }

        template<typename T>
        Triangle2D<T>::~Triangle2D(){
            delete _top;
            delete _bottomLeft;
            delete _bottomRight;
            _top = nullptr;
            _bottomLeft = nullptr;
            _bottomRight = nullptr;
        }

        template<typename T>
        Point2D<T>* Triangle2D<T>::getTopPoint() const {
            return _top;
        }

        template<typename T>
        Point2D<T>* Triangle2D<T>::getBottomLeftPoint() const {
            return _bottomLeft;
        }

        template<typename T>
        Point2D<T>* Triangle2D<T>::getBottumRightPoint() const {
            return _bottomRight;
        }

    }

}

#endif // TRIANGLE2D_H
