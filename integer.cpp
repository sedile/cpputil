#include <stdexcept>

#include "integer.h"

using namespace sed::wrap;

Integer::Integer(int value) :
    _value(new int(value))
{ }

Integer::Integer(const Integer *integerObject) {
    if ( integerObject == nullptr ){
        _value = new int(0);
    } else {
        _value = new int(integerObject->getValue());
    }
}

Integer::Integer(const Integer &cpy) :
    _value(new int(cpy.getValue()))
{ }

Integer& Integer::operator =(const Integer &ma){
    _value = new int(ma.getValue());
    return *this;
}

Integer::~Integer(){
    delete _value;
    _value = nullptr;
}

std::string Integer::intToString(){
    return std::string(std::to_string(*_value));
}

std::string Integer::intToString(const int value){
    return std::string(std::to_string(value));
}

Integer Integer::parseInteger(const Short &obj){
    return wrap::Integer(static_cast<int>(obj.getValue()));
}

Integer Integer::parseInteger(const UShort &obj){
    return wrap::Integer(static_cast<int>(obj.getValue()));
}

Integer Integer::parseInteger(const std::string &obj){
    int x = 0;
    try {
        x = std::stoi(obj);
        if ( x >= MIN_VALUE && x <= MAX_VALUE){
            return wrap::Integer(x);
        } else {
            return wrap::Integer(x);
        }
    } catch (const std::invalid_argument &){
        return wrap::Integer(x);
    } catch (const std::out_of_range &){
        return wrap::Integer(x);
    }
}

void Integer::deleteWrapper(){
    delete _value;
    _value = nullptr;
}

int Integer::getValue() const {
    return *(_value);
}
