#ifndef ULONG_H
#define ULONG_H

#include "ushort.h"
#include "uinteger.h"

namespace sed {

    namespace wrap {

    class ULong {
    private:
        unsigned long long *_value;
    public:
        static constexpr const unsigned long long MIN_VALUE = 0U;
        static constexpr const unsigned long long MAX_VALUE = 18446744073709551615uLL;

        explicit ULong(unsigned long long value);
        ULong(const ULong *ulongObject);
        ULong(const ULong &cpy);
        ULong& operator=(const ULong &ca);
        virtual ~ULong();

        std::string ulongToString();

        static std::string ulongToString(const unsigned long long value);
        static ULong parseULong(const UShort &obj);
        static ULong parseULong(const UInteger &obj);
        static ULong parseULong(const std::string &obj);

        void deleteWrapper();
        unsigned long long getValue() const;
    };

    }
}

#endif // ULONG_H
