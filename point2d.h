#ifndef POINT2D_H
#define POINT2D_H

namespace sed {

    namespace geometry {

        template<typename T = int>
        class Point2D {
        private:
            T _x;
            T _y;
        public:
            explicit Point2D();
            explicit Point2D(T x, T y);
            Point2D(const Point2D<T> &cpy);
            Point2D<T>& operator =(const Point2D<T> &mo) = delete;

            virtual ~Point2D();

            void setX(T x);
            void setY(T y);

            T getX() const;
            T getY() const;

        };

        template<typename T>
        Point2D<T>::Point2D() :
            _x(0), _y(0)
        { }

        template<typename T>
        Point2D<T>::Point2D(T x, T y) :
            _x(x), _y(y)
        { }

        template<typename T>
        Point2D<T>::Point2D(const Point2D<T> &cpy) :
            _x(cpy.getX()), _y(cpy.getY())
        { }

        template<typename T>
        Point2D<T>::~Point2D() { }

        template<typename T>
        void Point2D<T>::setX(const T x){
            _x = x;
        }

        template<typename T>
        void Point2D<T>::setY(const T y){
            _y = y;
        }

        template<typename T>
        T Point2D<T>::getX() const {
            return _x;
        }

        template<typename T>
        T Point2D<T>::getY() const {
            return _y;
        }
    }
}


#endif // POINT2D_H
