#ifndef IMPSTRING_H
#define IMPSTRING_H

#include <string>

namespace sed {

void substring(std::string &text, unsigned int start, unsigned int end){
    text = text.substr(start, (end+1)-start);
}

void replaceAll(std::string &original, const std::string oldChars, const std::string newChars){
    std::string result;
    result.reserve(original.length());

    std::string::size_type last = 0;
    std::string::size_type find;

    while( std::string::npos != ( find = original.find(oldChars, last))){
        result.append(original, last, find - last);
        result += newChars;
        last = find + oldChars.length();
    }

    result += original.substr(last);
    original.swap(result);
}

}

#endif // IMPSTRING_H
