#include <stdexcept>

#include "short.h"

using namespace sed::wrap;

Short::Short(short value) :
    _value(new short(value))
{ }

Short::Short(const Short *shortObject) {
    if ( shortObject == nullptr ){
        _value = new short(0);
    } else {
        _value = new short(shortObject->getValue());
    }
}

Short::Short(const Short &cpy) :
    _value(new short(cpy.getValue()))
{ }

Short& Short::operator =(const Short &ma){
    _value = new short(ma.getValue());
    return *this;
}

Short::~Short(){
    delete _value;
    _value = nullptr;
}

std::string Short::shortToString(){
    return std::string(std::to_string(*_value));
}

std::string Short::shortToString(const short value){
    return std::string(std::to_string(value));
}

Short Short::parseShort(const std::string &obj){
    short x = 0;
    try {
        x = std::stoi(obj);
        if ( x >= MIN_VALUE && x <= MAX_VALUE){
            return wrap::Short(x);
        } else {
            return wrap::Short(x);
        }
    } catch (const std::invalid_argument &){
        return wrap::Short(x);
    } catch (const std::out_of_range &){
        return wrap::Short(x);
    }
}

void Short::deleteWrapper(){
    delete _value;
    _value = nullptr;
}

short Short::getValue() const {
    return *(_value);
}
