#ifndef BOOL_H
#define BOOL_H

#include <string>

namespace sed {

    namespace wrap {

    class Bool {
    private:
        const bool *_value;
    public:
        explicit Bool(bool value);
        Bool(const Bool *boolObject);
        Bool(const Bool &cpy);
        Bool& operator=(const Bool &ca);
        virtual ~Bool();

        std::string boolToString();

        static Bool parseBool(const std::string &obj);
        static std::string boolToString(const bool value);

        void deleteWrapper();
        bool getValue() const;
    };

    }
}

#endif // BOOL_H
