#include <stdexcept>

#include "uinteger.h"

using namespace sed::wrap;

UInteger::UInteger(unsigned int value) :
    _value(new unsigned int(value))
{ }

UInteger::UInteger(const UInteger *uintegerObject) {
    if ( uintegerObject == nullptr ){
        _value = new unsigned int(0);
    } else {
        _value = new unsigned int(uintegerObject->getValue());
    }
}

UInteger::UInteger(const UInteger &cpy) :
    _value(new unsigned int(cpy.getValue()))
{ }

UInteger& UInteger::operator =(const UInteger &ma){
    _value = new unsigned int(ma.getValue());
    return *this;
}

UInteger::~UInteger(){
    delete _value;
    _value = nullptr;
}

std::string UInteger::uintToString(){
    return std::string(std::to_string(*_value));
}

std::string UInteger::uintToString(const unsigned int value){
    return std::string(std::to_string(value));
}

UInteger UInteger::parseUInteger(const UShort &obj){
    return wrap::UInteger(static_cast<unsigned int>(obj.getValue()));
}

UInteger UInteger::parseUInteger(const std::string &obj){
    unsigned int x = 0;
    try {
        x = static_cast<unsigned int>(std::stoull(obj));
        return wrap::UInteger(x);
    } catch (const std::invalid_argument &){
        return wrap::UInteger(x);
    } catch (const std::out_of_range &){
        return wrap::UInteger(x);
    }
}

void UInteger::deleteWrapper(){
    delete _value;
    _value = nullptr;
}

unsigned int UInteger::getValue() const {
    return *(_value);
}
