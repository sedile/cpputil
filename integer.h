#ifndef INTEGER_H
#define INTEGER_H

#include "short.h"
#include "ushort.h"

namespace sed {

    namespace wrap {

    class Integer {
    private:
        int *_value;
    public:
        static constexpr const int MIN_VALUE = -2147483648;
        static constexpr const int MAX_VALUE = 2147483647;

        explicit Integer(int value);
        Integer(const Integer *integerObject);
        Integer(const Integer &cpy);
        Integer& operator=(const Integer &ca);
        virtual ~Integer();

        std::string intToString();

        static std::string intToString(const int value);
        static Integer parseInteger(const Short &obj);
        static Integer parseInteger(const UShort &obj);
        static Integer parseInteger(const std::string &obj);

        void deleteWrapper();
        int getValue() const;
    };

    }
}

#endif // INTEGER_H
