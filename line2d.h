#ifndef LINE2D_H
#define LINE2D_H

#include <math.h>

#include "point2d.h"

namespace sed {

    namespace geometry {

        template<typename T = int>
        class Line2D {
        private:
            Point2D<T>* _start;
            Point2D<T>* _end;
        public:
            explicit Line2D();
            explicit Line2D(Point2D<T> &start, Point2D<T> &end);
            explicit Line2D(const Line2D<T> &cpy);
            Line2D<T>& operator=(const Line2D<T> &mo) = delete;

            virtual ~Line2D();

            double calculateDistance();

            Point2D<T>* getStartPoint() const;
            Point2D<T>* getEndPoint() const;

        };

        template<typename T>
        Line2D<T>::Line2D()
            : _start(new Point2D<T>), _end(new Point2D<T>)
        { }

        template<typename T>
        Line2D<T>::Line2D(Point2D<T> &start, Point2D<T> &end)
            : _start(new Point2D<T>(start)), _end(new Point2D<T>(end))
        { }

        template<typename T>
        Line2D<T>::Line2D(const Line2D<T> &cpy)
            : _start(new Point2D<T>(*cpy.getStartPoint())),
              _end(new Point2D<T>(*cpy.getEndPoint()))
        { }

        template<typename T>
        Line2D<T>::~Line2D(){
            delete _start;
            delete _end;

            _start = nullptr;
            _end = nullptr;
        }

        template<typename T>
        double Line2D<T>::calculateDistance(){
            double argOne = std::pow(_start->getX() - _end->getX(),2);
            double argTwo = std::pow(_start->getY() - _end->getY(),2);
            double value = argOne + argTwo;
            return std::sqrt(value);
        }

        template<typename T>
        Point2D<T>* Line2D<T>::getStartPoint() const {
            return _start;
        }

        template<typename T>
        Point2D<T>* Line2D<T>::getEndPoint() const {
            return _end;
        }

   }
}

#endif // LINE2D_H
