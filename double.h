#ifndef DOUBLE_H
#define DOUBLE_H

#include "float.h"

namespace sed {

    namespace wrap {

    class Double {
    private:
        double *_value;
    public:

        static constexpr const double MIN_VALUE = 2.22507e-308;
        static constexpr const double MAX_VALUE = 1.79769e+308;

        explicit Double(double value);
        Double(const Double *doubleObject);
        Double(const Double &cpy);
        Double& operator=(const Double &ca);
        virtual ~Double();

        std::string doubleToString();

        static std::string doubleToString(const double value);
        static Double parseDouble(const Float &obj);
        static Double parseDouble(const std::string &obj);

        void deleteWrapper();
        double getValue() const;
    };

    }

}

#endif // DOUBLE_H
