#ifndef SHORT_H
#define SHORT_H

#include <string>

namespace sed {

    namespace wrap {

    class Short {
    private:
        short *_value;
    public:
        static constexpr const short MIN_VALUE = -32768;
        static constexpr const short MAX_VALUE = 32767;

        explicit Short(short value);
        Short(const Short *shortObject);
        Short(const Short &cpy);
        Short& operator=(const Short &ca);
        virtual ~Short();

        std::string shortToString();

        static std::string shortToString(const short value);
        static Short parseShort(const std::string &obj);

        void deleteWrapper();
        short getValue() const;
    };

    }
}

#endif // SHORT_H
