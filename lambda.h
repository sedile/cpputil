#ifndef LAMBDA_H
#define LAMBDA_H

#include <functional>

namespace sed {

    namespace lambda {

        template<typename X, typename R>
        R function(const X &x, std::function<R(X)> expr ){
            return expr(x);
        }

        template<typename X, typename Y, typename R>
        R biFunction(const X &x, const Y &y, std::function<R(X,Y)> expr ){
            return expr(x, y);
        }

        template<typename X, typename Y, typename Z, typename R>
        R triFunction(const X &x, const Y &y, const Z &z, std::function<R(X,Y)> expr ){
            return expr(x, y, z);
        }

        template<typename X>
        void consumer(X &x, std::function<void(X)> expr ){
            return expr(x);
        }

        template<typename X, typename Y>
        void biconsumer(X &x, Y &y, std::function<void(X,Y)> expr ){
            return expr(x, y);
        }

        template<typename X, typename Y, typename Z>
        void triconsumer(X &x, Y &y, Z &z, std::function<void(X,Y,Z)> expr ){
            return expr(x, y, z);
        }

        template<typename R>
        R create(std::function<R()> expr ){
            return expr();
        }
    }

}

#endif // LAMBDA_H
