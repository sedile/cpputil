#ifndef FLOAT_H
#define FLOAT_H

#include <string>

namespace sed {

    namespace wrap {

    class Float {
    private:
        float *_value;
    public:

        static constexpr const float MIN_VALUE = 1.17549e-38;
        static constexpr const float MAX_VALUE = 3.40282e+38;

        explicit Float(float value);
        Float(const Float *floatObject);
        Float(const Float &cpy);
        Float& operator=(const Float &ca);
        virtual ~Float();

        std::string floatToString();

        static std::string floatToString(const float value);
        static Float parseFloat(const std::string &obj);

        void deleteWrapper();
        float getValue() const;
    };

    }
}

#endif // FLOAT_H
