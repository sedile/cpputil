#ifndef UCHAR_H
#define UCHAR_H

#include <string>

namespace sed {

    namespace wrap {

    class UChar {
    private:
        unsigned char *_value;
    public:
        explicit UChar(unsigned char value);
        UChar(const UChar *boolObject);
        UChar(const UChar &cpy);
        UChar& operator=(const UChar &ca);
        virtual ~UChar();

        std::string ucharToString();

        static std::string ucharToString(const unsigned char value);

        void deleteWrapper();
        unsigned char getValue() const;
    };

    }
}

#endif // UCHAR_H
