#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <string>
#include <iostream>

namespace sed {

struct LinkedListException {

    LinkedListException(const std::string &err){
        std::cout << "LinkedListException : " << err << std::endl;
    }
};

template<typename T> class ListNode {
private:
    T _data;
    ListNode<T> *_next;
public:
    explicit ListNode();
    explicit ListNode(T data);
    virtual ~ListNode();

    void setData(T data);
    void setNext(ListNode<T> *node);

    T getData() const;
    ListNode<T>* getNext() const;

    size_t getMemory() const;
};

template<typename T>
ListNode<T>::ListNode() :
    _data(nullptr),
    _next(nullptr)
    {}

template<typename T>
ListNode<T>::ListNode(T data) :
    _data(data),
    _next(nullptr)
    {}

template<typename T>
ListNode<T>::~ListNode() {
    delete _next;
}

template<typename T>
void ListNode<T>::setData(T data){
    _data = data;
}

template<typename T>
void ListNode<T>::setNext(ListNode<T> *next){
    _next = next;
}

template<typename T>
T ListNode<T>::getData() const {
    return _data;
}

template<typename T>
ListNode<T>* ListNode<T>::getNext() const {
    return _next;
}

template<typename T>
size_t ListNode<T>::getMemory() const {
    return sizeof(this);
}

template<typename T> class LinkedList {
private:
    ListNode<T> *_root, *_end;
public:
    explicit LinkedList();
    explicit LinkedList(const LinkedList<T> &copy) = delete;
    explicit LinkedList(LinkedList<T> &&mv) = delete;
    virtual ~LinkedList();

    void add(T data);
    void insertAt(T data, unsigned int);
    void insertFirst(T data);
    void insertLast(T data);
    T getFirst() const;
    T getLast() const;
    bool removeData(T);
    bool removeFirst();
    bool removeLast();
    bool removeAt(unsigned int);
    bool changeDataAt(T data, unsigned int);
    T getDataAt(unsigned int) const;
    unsigned int getSize() const;
    bool isEmpty() const;
    int findData(T) const;
    bool clear();
    void arrayToList(T*, unsigned int);
    T* listToArray() const;
    LinkedList<T>* getCopy() const;
    size_t getMemory() const;
};

template<typename T>
LinkedList<T>::LinkedList() :
    _root(nullptr),
    _end(nullptr)
    {}

template<typename T>
LinkedList<T>::~LinkedList() {
    delete _root;
    delete _end;
    _root = nullptr;
    _end = nullptr;
}

/* Fuegt ein Element in das Listenende ein */
template<typename T>
void LinkedList<T>::add(T data) {
    ListNode<T> *elem = new ListNode<T>(data);
    if (_root == nullptr) {
        _root = elem;
        _end = elem;
        return;
    }
    else {
        _end->setNext(elem);
        _end = elem;
        return;
    }
}

/* Fuegt ein Element in eine bestimmte Position in der Liste ein */
template<typename T>
void LinkedList<T>::insertAt(T data, unsigned int index) {
    if (index > getSize() || isEmpty()) {
        return;
    }

    /* wenn der Index 0 ist, ist dies der Startknoten der Liste */
    if (index == 0) {
        insertFirst(data);
        return;
    }

    /* wenn der Index getSize() ist, ist dies der Endknoten der Liste */
    if (index > getSize()) {
        insertLast(data);
        return;
    }

    /* wenn der Index zwischen 0 und getSize() ist, fuege Knoten in der Liste ein */
    ListNode<T> *elem = new ListNode<T>(data);
    ListNode<T>* temp = _root;

    /* Suche EinfÃ¼geposition */
    for (unsigned int i = 0; i < index; i++) {
        temp = temp->getNext();
    }

    /* Verbinde neues Element mit bereits bestehenden Knoten in der Liste */
    elem->setNext(temp);

    /* Gehe zurÃ¼ck zum Start und zur Position, bevor das Element eingefÃ¼gt wurde */
    temp = _root;

    for (unsigned int i = 0; i < index - 1; i++) {
        temp = temp->getNext();
    }

    /* Verbinde das vorherige Element mit den neuen Element */
    temp->setNext(elem);
}

/* Fuegt ein Element an den Listenanfang ein */
template<typename T>
void LinkedList<T>::insertFirst(T data) {
    if (_root == nullptr) {
        add(data);
        return;
    }

    ListNode<T> *elem = new ListNode<T>(data);
    elem->setNext(_root);
    _root = elem;
}

/* Fuegt ein Element in das Listenende ein */
template<typename T>
void LinkedList<T>::insertLast(T data) {
    if (_root == nullptr) {
        add(data);
        return;
    }

    ListNode<T> *elem = new ListNode<T>(data);
    _end->setNext(elem);
    _end = elem;
}

/* Gibt das erste Element aus der Liste als Kopie zurueck */
template<typename T>
T LinkedList<T>::getFirst() const {
    ListNode<T> *temp = _root;
    T result;

    if (temp != nullptr) {
        result = temp->getData();
        return result;
    }
    else {
        throw new LinkedListException("list is empty");
    }
}

/* Gibt das letzt Element aus der Liste als Kopie zurueck */
template<typename T>
T LinkedList<T>::getLast() const {
    ListNode<T> *temp = _root;
    T result;

    while (temp != nullptr) {
        if (temp->getNext() == nullptr) {
            result = temp->getData();
            return result;
        }
        temp = temp->getNext();
    }
    throw new LinkedListException("list is empty");
}

/* Entfernt das uebergebene Element, falls vorhanden */
template<typename T>
bool LinkedList<T>::removeData(T data) {
    if (_root == nullptr) {
        return false;
    }

    ListNode<T> *temp = _root;
    ListNode<T> *con = nullptr;
    while (temp->getNext() != nullptr) {
        if (temp->getNext()->getData() == data) {
            con = temp->getNext()->getNext();
            temp->setNext(con);
            return true;
        }
        else {
            temp = temp->getNext();
        }
    }

    return false;
}

/* Entfernt das erste Element aus der Liste */
template<typename T>
bool LinkedList<T>::removeFirst() {
    if (_root == nullptr) {
        return false;
    }

    ListNode<T> *temp = _root;
    _root = _root->getNext();
    temp->setNext(nullptr);
    temp = nullptr;
    delete temp;
    return true;
}

/* Entfernt das letzte Element aus der Liste */
template<typename T>
bool LinkedList<T>::removeLast() {
    if (_root == nullptr) {
        return false;
    }

    ListNode<T> *temp = _root;
    while (temp->getNext() != nullptr) {
        _end = temp;
        temp = temp->getNext();
    }
    _end->setNext(nullptr);
    delete temp;
    return true;
}

/* Entfernt ein Element an einer bestimmten Stelle */
template<typename T>
bool LinkedList<T>::removeAt(unsigned int index){
    if (_root == nullptr || index >= getSize()){
        return false;
    }

    if( index == 0 ){
        return removeFirst();
    } else if ( index == getSize() - 1){
        return removeLast();
    }

    ListNode<T> *temp = _root;
    while(temp->getNext() != nullptr && (index-1) > 0){
        index--;
        temp = temp->getNext();
    }

    ListNode<T> *n = temp->getNext()->getNext();
    if( n != nullptr){
        temp->setNext(n);
    } else {
        return false;
    }
    return true;
}

/* Sucht, ob sich das uebergebe Element sich in der Liste befindet und gibt falls vorhanden
   den Index zurueck. Falls nicht vorhanden -1 */
template<typename T>
int LinkedList<T>::findData(T data) const {
    ListNode<T> *temp = _root;
    int idx = 0;
    while (temp != nullptr) {
        if (temp->getData() == data) {
            return idx;
        }
        else {
            temp = temp->getNext();
            idx++;
        }
    }
    delete temp;
    return -1;
}

/* Veraendert ein Eintrag in der Liste an der uebergebenen Position */
template<typename T>
bool LinkedList<T>::changeDataAt(T data, unsigned int index) {
    if (index > getSize() - 1 || isEmpty()) {
        return false;
    }

    ListNode<T> *temp = _root;
    while (index > 0) {
        temp = temp->getNext();
        index--;
    }

    temp->setData(data);
    return true;
}

/* Gibt das Element an der uebergebenen Position zurueck */
template<typename T>
T LinkedList<T>::getDataAt(unsigned int index) const {
    T result;
    if (index > getSize() - 1 ) {
        throw new LinkedListException(index+" is an invalid index");
    } else if ( isEmpty() ){
        throw new LinkedListException("list is empty");
    }

    ListNode<T> *temp = _root;
    while (index > 0) {
        temp = temp->getNext();
        index--;
    }
    result = temp->getData();
    return result;
}

/* Gibt die Anzahl der Elemente aus der Liste zurueck */
template<typename T>
unsigned int LinkedList<T>::getSize() const {
    ListNode<T> *temp = _root;
    int elements = 0;
    while (temp != nullptr) {
        elements++;
        temp = temp->getNext();
    }
    delete temp;
    return elements;
}

/* Prueft, ob die Liste leer ist */
template<typename T>
bool LinkedList<T>::isEmpty() const {
    if (_root == nullptr) {
        return true;
    }
    return false;
}

/* Loescht alle Eintraege aus der Liste */
template<typename T>
bool LinkedList<T>::clear() {
    ListNode<T> *delPointer = _root;
    while (delPointer != nullptr) {
        _root = _root->getNext();
        delPointer = _root;
    }
    delete delPointer;
    delPointer = nullptr;

    if (isEmpty() && getSize() == 0) {
        return true;
    }
    return false;
}

/* Konvertiert ein Array als Liste, indem alle Arrayeintraege in die Liste eingefuegt werden und
   loescht bei Bedarf das uebergebene Array */
template<typename T>
void LinkedList<T>::arrayToList(T *arr, unsigned int arraysize) {
    for (unsigned int i = 0; i < arraysize; i++) {
        add(*(arr + i));
    }
}

/* Wandelt die Liste in ein Array um und gibt es als Pointer zurueck */
template<typename T>
T* LinkedList<T>::listToArray() const {
    T *t = new T[getSize()];
    ListNode<T> *temp = _root;
    unsigned int counter = getSize();
    for (unsigned int i = 0; i < getSize(); i++) {
        *(t + i) = temp->getData();
        counter--;
        temp = temp->getNext();
    }
    delete temp;
    if (counter > 0) {
        throw new LinkedListException("error while convert the list to an array");
    }
    return t;
}

/* Erzeugt eine Kopie der aktuellen Liste und gibt diese Kopie zurueck */
template<typename T>
LinkedList<T>* LinkedList<T>::getCopy() const {
    LinkedList<T> *cpy = new LinkedList<T>();
    ListNode<T> *temp = _root;
    while (temp != nullptr) {
        cpy->add(temp->getData());
        temp = temp->getNext();
    }

    if (getSize() != cpy->getSize()) {
        throw new LinkedListException("internal error");
    }

    return cpy;
}

/* Gibt den aktuellen Speicherverbrauch der Liste samt ihrere Elemente an */
template<typename T>
size_t LinkedList<T>::getMemory() const {
    size_t memoryUsage = 0;
    ListNode<T> *temp = _root;
    while(temp != nullptr){
        memoryUsage += temp->getMemory();
        temp = temp->getNext();
    }
    return memoryUsage + sizeof(this);
}

}

#endif // LINKEDLIST_H
