#include <stdexcept>

#include "long.h"

using namespace sed::wrap;

Long::Long(long long value) :
    _value(new long long(value))
{ }

Long::Long(const Long *longObject) {
    if ( longObject == nullptr ){
        _value = new long long(0);
    } else {
        _value = new long long(longObject->getValue());
    }
}

Long::Long(const Long &cpy) :
    _value(new long long(cpy.getValue()))
{ }

Long& Long::operator =(const Long &ma){
    _value = new long long(ma.getValue());
    return *this;
}

Long::~Long(){
    delete _value;
    _value = nullptr;
}

std::string Long::longToString(){
    return std::string(std::to_string(*_value));
}

std::string Long::longToString(const long value){
    return std::string(std::to_string(value));
}

Long Long::parseLong(const Short &obj){
    return wrap::Long(static_cast<long>(obj.getValue()));
}

Long Long::parseLong(const UShort &obj){
    return wrap::Long(static_cast<long>(obj.getValue()));
}

Long Long::parseLong(const Integer &obj){
    return wrap::Long(static_cast<long>(obj.getValue()));
}

Long Long::parseLong(const UInteger &obj){
    return wrap::Long(static_cast<long>(obj.getValue()));
}

Long Long::parseLong(const std::string &obj){
    long long x = 0;
    try {
        x = std::stoll(obj);
        if ( x >= MIN_VALUE && x <= MAX_VALUE){
            return wrap::Long(x);
        } else {
            return wrap::Long(x);
        }
    } catch (const std::invalid_argument &){
        return wrap::Long(x);
    } catch (const std::out_of_range &){
        return wrap::Long(x);
    }
}

void Long::deleteWrapper(){
    delete _value;
    _value = nullptr;
}

long long Long::getValue() const {
    return *(_value);
}
